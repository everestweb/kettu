new Chartist.Line('.average-chart', {
	labels: (function() {
        var labels = [];
        var i = 0;

        for(; i <=30; i++) labels.push(i);

        return labels;
    }()),
	series: [(function() {
        var labels = [];
        var i = 0;

        for(; i <=30; i++) labels.push(i);

        return labels;
    }())
	]
}, {
	low: 0,
	showArea: true
});

var $chart = $('.average-chart');

var $toolTip = $chart
	.append('<div class="tooltip"></div>')
	.find('.tooltip')
	.hide();

$chart.on('mouseenter', '.ct-point', function() {
	var $point = $(this),
		value = $point.attr('ct:value'),
		seriesName = $point.parent().attr('ct:series-name');
	$toolTip.html(seriesName + '<br>' + value).show();
});

$chart.on('mouseleave', '.ct-point', function() {
	$toolTip.hide();
});

$chart.on('mousemove', function(event) {
	$toolTip.css({
		left: (event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2 - 10,
		top: (event.offsetY || event.originalEvent.layerY) - $toolTip.height() - 40
	});
});

new Chartist.Bar('.team-average-chart', {
	labels: ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta'],
	series: [
		[60000, 40000, 80000, 70000],
		[40000, 30000, 70000, 65000],
		[8000, 3000, 10000, 6000],
		[8000, 3000, 10000, 6000]
	]
}, {
	seriesBarDistance: 10,
	axisX: {
		offset: 60
	},
	axisY: {
		offset: 80,
		scaleMinSpace: 15
	}
});
