;
(function($) {

	'use strict';

	var chatbox = {

		base_url: 'chat/',
		api_url: 'api/',
		send_url: 'send/',
		token: '',
		is_binded: true,

		init: function() {

			$('#submit').click(function(event) {
				event.preventDefault();

				var message = $('#message').val();

				$('#controls').submit();

				chatbox.post_message(message);
			});

			$('#controls').submit(function(event) {
				event.preventDefault();

				var message = $('#message').val();

				chatbox.post_message(message);

				$('#message').val('');
			});

			var check_messages = setInterval(function() {
				chatbox.update_message();
			}, 2000);

			$('#messages').bind('DOMSubtreeModified', function() {
				chatbox.bind();
			});

		},

		bind: function() {
			chatbox.edit_message();
		},

		get_messages: function(token) {
			$.get(chatbox.base_url + chatbox.api_url + token, function(data) {
				chatbox.set_messages(data);
			});
		},

		post_message: function(message) {
			$.post(chatbox.base_url + chatbox.send_url, {
				message: message
			}).success(function(data) {
				chatbox.update_message(data);
			}).error(function() {
				alert('erro');
			});
		},

		update_message: function() {
			$.get(chatbox.base_url + chatbox.api_url + chatbox.token, function(data) {
				var len = $('#messages > li').length;
				var message = data['messages']['messages'];

				if (message.length > len) {
					var message = message[(message.length - 1)];
					// console.info(message['status']);

					$('#chat-main #messages').prepend(function() {
						var line =
							'<li class="line clearfix" id="' + message['id'] + '">' +
							'	<div class="avatar"><img src="assets/img/avatar/' + message['avatar'] + '" data-toggle="tooltip" data-placement="top" title="' + message['author'] + '"></div>' +
							'	<div class="message">' + message['message']['original'] + '</div>' +
							'</li>';

						return line;
					});

					$('[data-toggle="tooltip"]').tooltip();

					document.getElementById('notifications').play();
				};
			});
		},

		set_messages: function(data) {
			$.when(data).done(function(messages) {
				$('#loading').fadeOut(function() {
					var deleted = $(this).data('timestamp');
					$(this).parent().html('');

					$.each(messages['messages']['messages'], function(index, message) {
						chatbox.add_message(message);
					});
				});
			});
		},

		edit_message: function() {
			$('#messages li.line .message').unbind('dblclick').dblclick(function() {
				$(this).attr('contenteditable', true).addClass('editing');
			});
		},

		add_message: function(message) {

			if (message['message']['edited'].trim() === '') {
				$('#chat-main #messages').prepend(function() {
					// implementar função para criar a li pelo javascript e não como string.
					var line =
						'<li class="line clearfix" id="' + message['id'] + '">' +
						'	<div class="avatar"><img src="assets/img/avatar/' + message['avatar'] + '" data-toggle="tooltip" data-placement="top" title="' + message['author'] + '"></div>' +
						'	<div class="message">' + message['message']['original'] + '</div>' +
						'</li>';

					return line;
				});

				$('[data-toggle="tooltip"]').tooltip();
				$('pre code').each(function(i, block) {
					hljs.highlightBlock(block);
				});

			} else {
				console.info(message['edited']);
			}
		}
	};


	chatbox.init();

	$(document).on('read-token', function(event, token) {
		chatbox.token = token;
		chatbox.get_messages(token);
	});

	$('pre code').each(function(i, block) {
		hljs.highlightBlock(block);
	});

})(jQuery);
