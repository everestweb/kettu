<nav class="navbar navbar-inverse clearfix">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>
    <div class="collapse navbar-collapse" id="navbar-header">
        <ul class="nav navbar-nav">
            <?php if(!$this->session->has_userdata('is_logged')): ?>
            <li class="active">
                <?php echo anchor(base_url('chat'), 'Login'); ?>
            </li>
            <li>
                <?php echo anchor(base_url('chat/register'), 'Register'); ?>
            </li>
            <?php endif; ?>
        </ul>

        <?php if($this->session->has_userdata('is_logged')): ?>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <?php echo anchor(base_url('chat/users/' . $this->session->userdata('username')), $this->session->userdata('username') . '<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"', 'ok'); ?>
                <ul class="dropdown-menu">
                    <li>
                        <?php echo anchor(base_url('chat/logout'), 'Logout'); ?>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif; ?>
    </div>
</nav>
