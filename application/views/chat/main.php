<section id="mobile">
    <header id="chat-header">
        <?php $this->load->view('chat/navbar'); ?>
    </header>
    <main id="chat-main">
        <?php $this->load->view('chat/alert-messages'); ?>
        <div class="container-fluid">
            <?php if($this->session->has_userdata('is_logged')): ?>
            <ul id="messages">
                <li id="loading" data-timestamp="<?php echo time(); ?>">Carregando...</li>
            </ul>
            <?php
                else:
                    $this->load->view('chat/login');
                endif;

                if($this->session->has_userdata('is_logged')) {
                    $this->load->view('chat/controls');
                }
            ?>
        </div>
    </main>
</section>
