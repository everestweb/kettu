<div id="alert-messages">
    <?php if($this->session->flashdata('logged')): ?>
    <div class="alert alert-success alert-dismissible" role="alert" id="status-message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $this->session->flashdata('logged'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('logout')): ?>
    <div class="alert alert-warning alert-dismissible" role="alert" id="status-message">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $this->session->flashdata('logout'); ?>
    </div>
    <?php endif; ?>
</div>
