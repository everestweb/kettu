<?php echo form_open(base_url('chat/send'), 'id="controls"', array(
    "timestamp" => time()
)); ?>
    <div class="form-group">
        <div class="input-group">
            <?php echo form_input('message', '', 'class="form-control" id="message" placeholder="Digite sua mensagem" maxlength="200" autocomplete="off"'); ?>
            <div id="submit" class="input-group-addon"><i class="fa fa-send"></i></div>
        </div>
    </div>
<?php echo form_close(); ?>
