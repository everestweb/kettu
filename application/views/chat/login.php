<?php
    echo form_open('chat/login', 'id="chat-login"');
    echo validation_errors('<div class="form-group"><span class="error">', '</span></div>');
?>
    <div class="form-group">
        <label for="username">Usuário</label>

        <?php echo form_input(array(
            'type' => 'text',
            'class' => 'form-control',
            'size' => 40,
            'name' =>'username',
            'id' => 'username',
            'placeholder' => 'Digite seu nome de usuário ou nickname',
            'maxlength' => 40,
            'value'=>'waghcwb'
        )); ?>
    </div>
    <div class="form-group">
        <label for="password">Senha</label>

        <?php
            echo form_password(array(
                'class' => 'form-control',
                'size' => 40,
                'name'=> 'password',
                'id' => 'password',
                'placeholder' => 'Digite sua senha',
                'maxlength' => 40
            ));
        ?>

    </div>

    <?php
        echo form_button(array(
            'name' => 'submit',
            'value' => hash('md5', time()),
            'type' => 'submit',
            'content'=> 'Enviar',
            'class' => 'btn btn-primary btn-block'
        ));

        echo form_close();

    ?>
