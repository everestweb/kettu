<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>

    <?php load_common(true); ?>

    <link href="<?php echo base_url('assets/libs/css/monokai_sublime.css'); ?>" rel="stylesheet" type="text/css">
</head>
<body>
