


    <footer>
    </footer>


    <audio id="notifications">
        <source src="<?php echo base_url('assets/sounds/new-message.mp3');?>" type="audio/mpeg">
        <source src="<?php echo base_url('assets/sounds/new-message.ogg');?>" type="audio/ogg">
    </audio>

    <script src="<?php echo base_url('assets/common/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/common/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/libs/js/highlight.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/pages/chat.js'); ?>"></script>
    <?php if($this->session->has_userdata('is_logged')): ?>
    <script>
        var userdata = [];
            userdata['token'] = "<?php echo $this->session->userdata('token'); ?>";

        $(document).trigger('read-token', userdata['token']);
    </script>
    <?php endif; ?>
</body>
</html>
