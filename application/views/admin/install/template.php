<head>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
</head>
<section id="install">
    <section class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Processo de instalação do servidor</div>
            <div class="panel-body">
                <!-- TODO melhorar visual do alerta -->
                <?php
                    $username = array(
                        'name' => 'username',
                        'id' => 'username',
                        'value' => '',
                        'maxlength' => '40',
                        'size' => '50',
                    );
                    
                    echo form_open('install');
                ?>
                <?php if ($_SESSION["setup_success"]): ?>
                    <div class="form-group alert bg-success">
                        Banco de dados configurado com sucesso!
                    </div>
                <?php else: ?>
                    <div class="form-group alert alert-warning">
                        <?php echo $_SESSION["database_info"]["database_error"] ?>
                    </div>
                <?php endif; ?>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="database-name">Nome do banco de dados</label>
                            <input type="text" id="database-name" class="form-control" value="<?php echo $_SESSION["database_info"]['database_name'] ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="database-user">Usuário do banco de dados</label>
                            <input type="text" id="database-user" class="form-control" value="<?php echo $_SESSION["database_info"]['database_user'] ?>" readonly>
                        </div>
                    </div>
                    <?php if (!$_SESSION["setup_success"]): ?>
                        <div class="form-group">
                            <label class="" for="setup_password">Senha para instalação do sistema</label>

                            <div class="input-group">
                                <input type="password" class="form-control" id="setup_password" name="setup_password"
                                       placeholder="Por favor, digite a senha para a instalação">

                                <div class="input-group-addon">
                                    <a href="#see-password">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <?php if ($_SESSION["setup_success"]): ?>
                            <a class="bnt btn-primary btn-block" href="/">Página inicial</a>
                        <?php else: ?>
                            <button type="submit" class="btn btn-primary btn-block">Iniciar instalação</button>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
    </section>
</section>
