<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>
		<?php echo $title; ?>
	</title>
</head>

<body>
	<section class="container">
		<form action="<?php echo base_url('/login'); ?>" method="post" style="max-width:50%;margin:2em auto;">
			<div class="form-group">
				<label for="user">Usuário</label>
				<input type="text" class="form-control" id="user" placeholder="username">
			</div>
			<div class="form-group">
				<label for="pass">Senha</label>
				<input type="password" class="form-control" id="pass" placeholder="Senha">
			</div>
			<button type="submit" class="btn btn-primary btn-block">Enviar</button>
		</form>
	</section>
</body>

</html>
