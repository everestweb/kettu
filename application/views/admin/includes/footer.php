
    <footer>
        <section class="container">
            <?php
                echo VERSION;
            ?>
        </section>
    </footer>
    <script src="<?php echo base_url('assets/common/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/common/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/libs/js/chartist.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/pages/admin.js'); ?>"></script>

    </script>
</body>
</html>
