<!DOCTYPE html>
<html class="no-js" lang="<?php echo (isset($lang) ? $lang : $this->config->item('site_info')['lang']); ?>">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title; ?></title>

	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url('assets/libs/css/chartist.min.css'); ?>" rel='stylesheet' type='text/css'>

	<script src="<?php echo base_url('assets/common/js/modernizr-2.8.3.min.js') ?>"></script>
</head>
<body>
