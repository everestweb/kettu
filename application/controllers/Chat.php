<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller
{
    private $chatbox = array();
    private $title   = 'Chatbox';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('chat_model');
        $this->load->helper(['file', 'form', 'security', 'url', 'bbcode']);
        $this->load->library(['css', 'session']);

        //beta teste
        $messages = json_decode($this->chat_model->read_messages(), true);

        $user_messages = $messages['messages'];

        foreach ($user_messages as $k => $v) {
            $user_messages[$k]['message']['original'] = show_bb_codes($user_messages[$k]['message']['original']);
        }

        $messages['messages'] = $user_messages;

        $this->chatbox = array(
            'messages' => $messages,
            'users' => json_decode($this->chat_model->get_users(), true)
        );
    }

    public function index()
    {
        $data = array(
            'title' => '',
        );

        $this->load->view('chat/includes/head', $data);
        $this->load->view('chat/header');
        $this->load->view('chat/main', $this->chatbox);
        $this->load->view('chat/includes/footer');
    }

    public function login()
    {
        $password = $this->chatbox['users'][$this->input->post('username')]['password'];

        if ($this->chatbox['users'][$this->input->post('username')] AND password_verify($this->input->post('password'), $password)) {
            $this->session->set_userdata(array(
                'is_logged' => 1,
                'username' => $this->input->post('username'),
                'token' => $this->chatbox['users'][$this->input->post('username')]['token'],
                'avatar' => $this->chatbox['users'][$this->input->post('username')]['avatar'],
                'status' => $this->chatbox['users'][$this->input->post('username')]['status']
            ));
            $this->session->set_flashdata('logged', 'Seja bem vindo, ' . $this->session->userdata('username'));
            redirect(base_url('chat'), 'refresh');
        }
        else {
            header('HTTP/1.1 401 Unauthorized');
            header('Content-Type: application/json');
            echo json_encode(array(
                'status' => 'Usuário e/ou senha incorretos. Por favor, tente novamente'
            ));
        };
    }

    public function logout()
    {
        $this->chat_model->logout();
    }

    public function register()
    {
        $data = array(
            'title' => 'Chatbox'
        );

        $this->load->view('chat/includes/head', $data);
        $this->load->view('chat/header');
        $this->load->view('chat/main', $this->chatbox);
        $this->load->view('chat/includes/footer');
    }

    public function api($token = "Token inválido")
    {
        if ($this->session->has_userdata('is_logged') AND $token === $this->session->userdata('token')) {
            header('Content-Type: application/json');
            echo json_encode(array(
                'messages' => $this->chatbox['messages']
            ));
        }
        else {
            header('HTTP/1.1 401 Unauthorized');
            header('Content-Type: application/json');
            echo json_encode(['Acesso não autorizado.']);
        };
    }

    public function send()
    {
        $this->chat_model->post_message();
    }
}
