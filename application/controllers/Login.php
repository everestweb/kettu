<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    protected $data;

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(['url']);
        $this->data = array(
            'title' => 'Login'
        );
    }

	public function index()
	{
        $this->load->library('css');
        $this->css->load(array('main','css2'));
        $this->load->view('admin/includes/head', $this->data);
        $this->load->view('admin/login/login', $this->data);
	}

    public function teste()
    {
        echo 'ok';
    }
}
