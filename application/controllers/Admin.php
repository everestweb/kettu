<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library(['menu', 'library', 'css']);
        $this->load->helper(['url']);
        $this->load->model('Users');


    }

	public function index()
	{
        // $this->Users->only_master_admin();
        $this->load->view('admin/includes/head');
        $this->load->view('admin/header');
        $this->load->view('admin/main');
        $this->load->view('admin/includes/footer');
	}

    public function logout()
    {
        echo 'logout';
    }
}
