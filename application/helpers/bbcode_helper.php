<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('show_bb_codes')) {
    function show_bb_codes($text)
    {
        $find = array(
            '~\[b\](.*?)\[/b\]~s',
            '~\[i\](.*?)\[/i\]~s',
            '~\[u\](.*?)\[/u\]~s',
            '~\[del\](.*?)\[/del\]~s',
            '~\[quote\](.*?)\[/quote\]~s',
            '~\[size=(.*?)\](.*?)\[/size\]~s',
            '~\[color=(.*?)\](.*?)\[/color\]~s',
            '~\[code=(.*?)\](.*?)\[/code\]~s',
            '~\[url\]((?:ftp|http?)://.*?)\[/url\]~s',
            '~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s'
        );

        $replace = array(
            '<b>$1</b>',
            '<i>$1</i>',
            '<del>$1</del>',
            '<span style="text-decoration:underline;">$1</span>',
            '<pre>$1</' . 'pre>',
            '<span style="font-size:$1px;">$2</span>',
            '<span style="color:$1;">$2</span>',
            '<pre><code class=$1>$2</code></pre>',
            '<a href="$1">$1</a>',
            '<img src="$1" alt="" />'
        );

        return preg_replace($find, $replace, $text);
    }

}