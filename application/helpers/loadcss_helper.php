<?php defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('load_common')) {
    function load_common($true = True)
    {
        $ci =& get_instance();
        $ci->load->helper('directory', 'url');
        $ret = '<head>';
        foreach (directory_map('assets/common/css/') as $value) {
            if (pathinfo($value, PATHINFO_EXTENSION) == 'css') {
                $ret .= '<link rel="stylesheet" type="text/css" href="' . base_url('assets/common/css/' . $value) . '">';
            };
        }
        $custom_css = 'assets/css/pages/' . $ci->router->class . '.css';
        if (file_exists(FCPATH . $custom_css)) {
            $ret .= '<link rel="stylesheet" type="text/css" href="' . base_url($custom_css) . '">';
        };
        $ret .= '</head>';
        if ($true) {
            echo $ret;
            return true;
        } else {
            return $ret;
        }
    }
}