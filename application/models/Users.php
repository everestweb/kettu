<?php
class Users extends CI_Model
{

 public function __construct(){
     parent::__construct();
     $this->load->database('default');
 }

 public function only_master_admin(){

     if ($_SESSION['SUPER_USER'] !== defined('SUPER_USER') or $_SESSION['SUPER_PASSWORD'] !== defined('SUPER_PASSWORD')){
         header("Location: /");
         exit;
     };
 }

 public function is_logged(){

 }
}