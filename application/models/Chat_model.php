<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_model extends CI_Model
{

    public $messages;
    private $TAG_ALLOWED = "<a><div><img><p>";
    public function __construct()
    {
        parent::__construct();
    }

    public function read_messages()
    {
        return file_get_contents('data/chat/messages/messages.json');
    }

    public function get_users()
    {
        return file_get_contents('data/chat/users/users.json');
    }

    public function logout()
    {
        unset($_SESSION['is_logged'], $_SESSION['token'], $_SESSION['username']);

        $this->session->set_flashdata('logout', 'Você foi deslogado com sucesso.');

        redirect(base_url('chat'), 'refresh');

        exit;
    }

    public function post_message()
    {
        if(trim($this->input->post('message')) === '') {
            header('Content-Type: application/json');
            echo json_encode(['Sua mensagem está vazia.']);
            exit;
        }

        if($this->session->has_userdata('is_logged')) {

            $data = json_decode($this->chat_model->read_messages(), true);
            $timestamp = time();

            $data['info']['last_update'] = $timestamp;
            $data['info']['messages'] = count($data['messages']);

            $message = array(
                'id'        => MD5($this->session->userdata('username') . $timestamp),
                'timestamp' => $timestamp,
                'author'    => $this->session->userdata('username'),
                'token'     => $this->session->userdata('token'),
                'status'    => $this->session->userdata('status'),
                'avatar'    => $this->session->userdata('avatar'),
                'active'    => 1,
                'type'      => 'message',
                'message'   => array(
                    'original' =>html_escape($this->input->post('message')) ,
                    'edited'   => ''
                )
            );

            $data['messages'][] = $message;

            header('Content-Type: application/json');

            if (!write_file('data/chat/messages/messages.json', json_encode($data))) {
                echo json_encode(['Houve um erro ao salvar sua mensagem.']);
                exit;
            }
            else {
                echo $this->chat_model->read_messages();
            }

        }
        else {
            header('HTTP/1.1 401 Unauthorized');
            header('Content-Type: application/json');

            echo json_encode(['Acesso não autorizado.']);
        };


    }

}
