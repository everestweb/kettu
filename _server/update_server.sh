local_git="/server/everest"
remote_git="/server/everest-dev"
now=$(date +"%T")

cp -R $local_git/application $local_git/assets $local_git/backup $local_git/data $local_git/index.php $local_git/logs $local_git/system $remote_git

cd $remote_git

git add .
git commit -m "Update automático - $now"
git push

echo "new update: $now" >> update_$now
